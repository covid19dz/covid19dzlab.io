//const dailyStat = document.getElementById('dailyStat');
//const spinner  = document.getElementById('spinner');
moment.locale('fr');

// ------------------------------------------
//  FETCH FUNCTIONS
// ------------------------------------------
const api_url = 'https://covid19dz.openalgeria.org/api/';
const urls = [
    api_url + 'v1/stats',
    api_url + 'v1/wilayas',
    api_url + 'v1/ages/deaths',
    api_url + 'v1/sex',
    api_url + 'v2/history',
    './coordinates.json',
    api_url + 'v1/ages/cases',
    './wilaya_shape.json'
];

Promise.all(urls.map(url =>
  fetch(url)
    .then(checkStatus)                 
    .then(parseJSON)
    .catch(error => console.log('There was a problem! ' + url, error))
))
.then(data => {
  //spinner.className = "show";
  const stats = data[0];
  const wilayas = data[1];
  const ages_deaths = data[2];
  const sex = data[3];
  const historyTemp = data[4];
  const coordinate = data[5];
  const ages_cases = data[6];
  const limit_adm = data[7];
//   console.log(ages_cases);
//   console.log(ages_deaths);
  let wilayasWithCoordinates = wilayas.map((item, i) => Object.assign({}, item, coordinate[i]));

 var wilaya_actives_value = wilayas.map(function(e) {
    return { name: toTitleCase(e.name), value: e.actives};
    
});

let wilaya_coordinate_value = wilayasWithCoordinates.reduce((acc, cur) => ({ ...acc, [toTitleCase(cur.name)]: [cur.lon, cur.lat] }), {})
  
var maj = stats.dateAsOf;

let t1 = correctHistoryRecords(historyTemp);


for(let key in limit_adm.features){
    limit_adm.features[key].properties= wilayas[key];
    limit_adm.features[key].properties.name = toTitleCase(wilayas[key].name);
}
console.log('limite: ',limit_adm)
// ------------------------------------------
//   FUNCTIONS CALL
// ------------------------------------------   
  headerStats(stats);
  getMixedCasesByWilaya(wilayasWithCoordinates);
  getActivesCasesByWilaya(wilayasWithCoordinates);
  getDeathsCasesByWilaya(wilayas);
  getRecoveredCasesByWilaya(wilayas);
  getEvolution (t1);

  getMap2 (limit_adm)
 

//spinner.className = spinner.className.replace("show", "");

})

// ------------------------------------------
//  HELPER FUNCTIONS
// ------------------------------------------
 

function checkStatus(response) {
  if (response.ok) {
    return Promise.resolve(response);
  } else {
    return Promise.reject(new Error(response.statusText));
  }
}

function parseJSON(response) {
  return response.json();
}




var startDate = new Date("2020-1-22"); //YYYY-MM-DD
var endDate = new Date("2020-2-24"); //YYYY-MM-DD

var getDateArray = function(start, end) {
    var arr = new Array();
    var dt = new Date(start);
    while (dt <= end) {
        arr.push(new Date(dt));
        dt.setDate(dt.getDate() + 1);
    }
    return arr;
}

var datesToDelete = getDateArray(startDate, endDate);

function correctHistoryRecords (history){
    // removing invalid records from the beginning
   history = history.splice(33); 
   
   // test if the last record is 31 march then delete it
   if (history[history.length - 1].date == '2020-03-31'){
       history = history.splice(0, history.length - 1)
      
       
   }
   return history;
   
}


function getUniqueListBy(arr, key) {
    return [...new Map(arr.map(item => [item[key], item])).values()]
}

function headerStats(stats){
let headerhtml = `
<td style="color: #e60000;">${stats.confirmed.total}<sup class="text-muted" style="top: -.8em;font-size:50%;"> +${stats.confirmed.new}</sup></td>
<td  style="color: #ffaa00;">${stats.treatment}</td>
<td style="color: #38a800;">${stats.recovered.total}<sup class="text-muted" style="top: -.8em;font-size:50%;"> +${stats.recovered.new}</sup</td>
<td style="color: #e60000;">${stats.deaths.total}<sup class="text-muted" style="top: -.8em;font-size:50%;"> +${stats.deaths.new}</sup></td>
`;
dailyStat.innerHTML = headerhtml;
}

function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

function getEvolution (history){
    var evolutionValue = history.map(function(e) {
        return {x:new Date( e.date), y: e.confirmed};
     });
     moment().format("dd MMM YYYY");
    var ctx4 = document.getElementById('evolutionChart').getContext('2d');
     var myLineChart = new Chart(ctx4, {
        type: 'line',
        
        data: {
            labels: '',
            datasets: [{
              label: 'Cas confirmés',
              data: evolutionValue,
              backgroundColor: 'rgba(255, 99, 132, 0.2)',
              borderColor: 'rgba(255,99,132,1)',
               
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              xAxes: [{
                type: 'time',
                distribution: 'series',
                time: {
                    parser: 'DD/MMM/YYYY',
                    tooltipFormat: 'll',
                    unit: 'day',
                    unitStepSize: 1,
                    displayFormats: {
                    'day': 'DD MMM'
                    }
                }
              }]
            },
            maintainAspectRatio: false,
            responsive: true
          }
   });
}

function getActivesCasesByWilaya(wilayas){
    var actives_data = wilayas.map(function(e) {
        return  e.actives;
        
     });
     var wilaya_name = wilayas.map(function(e) {
        return toTitleCase(e.name);
        
     });
     //console.log(actives_data);
    var ctx = document.getElementById('activeChart').getContext('2d');
          var myChart = new Chart(ctx, {
              type: 'bar',
              data: {
                  labels: wilaya_name,
                  datasets: [{
                      label: 'Actives',
                      data: actives_data,
                      backgroundColor: 'rgba(255, 159, 64, 0.2)',
                      borderColor: 'rgba(255, 99, 132, 1)',
                      borderWidth: 1
                  }]
              },
              options: {
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero: true
                          }
                      }]
                  },
                  maintainAspectRatio: false,
                  responsive: true
              }
          });
}

function getMixedCasesByWilaya(wilayas){
    var actives_data = wilayas.map(function(e) {
        return  e.actives;
        
     });
    var recovered_data = wilayas.map(function(e) {
        return  e.recovered;
        
     });
    var deaths_data = wilayas.map(function(e) {
        return  e.deaths;
        
     });
     var wilaya_name = wilayas.map(function(e) {
        return toTitleCase(e.name);
        
     });
     //console.log(actives_data);
    var ctx = document.getElementById('mixedChart').getContext('2d');
          var myChart = new Chart(ctx, {
              type: 'bar',
              data: {
                  labels: wilaya_name,
                  datasets: [{
                      label: 'Actives',
                      data: actives_data,
                      backgroundColor: 'rgba(255, 159, 64, 0.2)',
                      borderColor: 'rgba(255, 159, 64, 0.2)',
                      borderWidth: 1
                  },
                    {
                    label: 'Rétablis',
                    data: recovered_data,
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 0.2)',
                    borderWidth: 1
                    },
                    
                    {
                    label: 'Décès',
                    data: deaths_data,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 0.2)',
                    borderWidth: 1
                  }]
              },
              options: {
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero: true
                          }
                      }]
                  },
                  maintainAspectRatio: false,
                  responsive: true
              }
          });
}


function getRecoveredCasesByWilaya(wilayas){
    var recovered_data = wilayas.map(function(e) {
        return  e.recovered;
        
     });
     var wilaya_name = wilayas.map(function(e) {
        return toTitleCase(e.name);
        
     });
     //console.log(actives_data);
    var ctx = document.getElementById('recoverededChart').getContext('2d');
          var myChart = new Chart(ctx, {
              type: 'bar',
              data: {
                  labels: wilaya_name,
                  datasets: [{
                      label: 'Actives',
                      data: recovered_data,
                      backgroundColor: 'rgba(75, 192, 192, 0.2)',
                      borderColor: 'rgba(75, 192, 192, 0.2)',
                      borderWidth: 1
                  }]
              },
              options: {
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero: true
                          }
                      }]
                  },
                  maintainAspectRatio: false,
                  responsive: true
              }
          });
}
function getDeathsCasesByWilaya(wilayas){
    var deaths_data = wilayas.map(function(e) {
        return  e.deaths;
        
     });
     var wilaya_name = wilayas.map(function(e) {
        return toTitleCase(e.name);
        
     });
     //console.log(actives_data);
    var ctx = document.getElementById('deathsChart').getContext('2d');
          var myChart = new Chart(ctx, {
              type: 'bar',
              data: {
                  labels: wilaya_name,
                  datasets: [{
                      label: 'Actives',
                      data: deaths_data,
                      backgroundColor: 'rgba(255, 99, 132, 0.2)',
                      borderColor: 'rgba(255, 99, 132, 0.2)',
                      borderWidth: 1
                  }]
              },
              options: {
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero: true
                          }
                      }]
                  },
                  maintainAspectRatio: false,
                  responsive: true
              }
          });
}

function getMap2 (limit_adm){
    
    var map2 = L.map('map3');

    L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/dark_all/{z}/{x}/{y}.png', {
        maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' 
	}).addTo(map2);
    map2.removeControl(map2.zoomControl);
    L.control.zoom({position: 'bottomright'}).addTo(map2);
    
    map2.setView(L.latLng(34.244, 2.791), 6);
    // control that shows state info on hover
	var info = L.control({position: 'topright'}); //L.control();

	info.onAdd = function (map2) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
    };
    info.setPosition('topleft')
    // console.log(info.getPosition());
     console.log(info.getContainer());

	info.update = function (props) {
        this._div.innerHTML =  (props ? `<div><h4 class="success">${props.name}</h4><h4 class="info">${props.name_ar}</h4></div>` : 'Survolez la carte');
	}
	 function getwilayaInfo(props) {
        return (props ? `<div><h4 class="success">${props.name}</h4><h4 class="info">${props.name_ar}</h4><table class="table-info">
        <tr>
          <td>Actives</td>
          <td>${((props.actives || props.actives == 'null')  ? props.actives  : 0)}</td>
        </tr>
        <tr>
          <td>Rétablis</td>
          <td>${((props.recovered || props.recovered == 'null')  ? props.recovered  : 0)}</td>
        </tr>
        <tr>
          <td>Décès</td>
          <td>${((props.deaths || props.deaths == 'null')  ? props.deaths  : 0)}</td>
        </tr>
        <tr>
          <td>Confirmés</td>
          <td>${((props.confirmed || props.confirmed == 'null')  ? props.confirmed  : 0)}</td>
        </tr>
        <tr>
          <td>Nouveaux</td>
          <td>${((props.new_cases || props.new_cases == 'null')  ? props.new_cases  : 0)}</td>
        </tr>
        <tr>
          <td>N. décès</td>
          <td>${((props.new_cases_death || props.new_cases_death == 'null')  ? props.new_cases_death  : 0)}</td>
        </tr>
      </table></div>` : 'Survolez la carte');
	}
  function getwilayaChart(props){
    var options = '';
    var ctx = document.getElementById('WilayaChart').getContext('2d');

    var data = {
        labels: ["Active", "Recovered","Deaths"],
          datasets: [
            {
                fill: true,
                backgroundColor: [
                    'orange',
                    'green',
                    'red'],
                data: [props.actives, props.recovered, props.deaths],
    // Notice the borderColor 
                borderColor:	['orange', 'green','red'],
                borderWidth: [1,1]
            }
        ]
    };
    
    // Notice the rotation from the documentation.
    
    var options = {
            title: {
                      display: true,
                      text: `${props.name} || ${props.name_ar}`,
                      position: 'top'
                  },
            rotation: -0.7 * Math.PI
    };
    
    
    // Chart declaration:
    var myBarChart = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: options
    });
  }
  function getwilayaChartSex(props){
    var options = '';
    var ctx = document.getElementById('WilayaChartSex').getContext('2d');

    var data = {
        labels: ["Masculin", "Féminin"],
          datasets: [
            {
                fill: true,
                backgroundColor: [
                    'blue',
                    'pink'],
                data: [props.sex.male,props.sex.female],
    // Notice the borderColor 
                borderColor:	['blue', 'pink'],
                borderWidth: [1,1]
            }
        ]
    };
    
    // Notice the rotation from the documentation.
    
    var options = {
            title: {
                      display: true,
                      text: `Nombre de cas par sexe`,
                      position: 'top'
                  },
            rotation: -0.7 * Math.PI
    };
    
    
    // Chart declaration:
    var myBarChart = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: options
    });
  }
/*  
      */
    info.addTo(map2);

    var data = [];
    for (let key in limit_adm.features){
        data[key] = limit_adm.features[key].properties;
    }
    console.log('data: ',data)
    function mxv(tableau, cle){
        let v = tableau.map(function(e) {
            return e[cle];
         });
         
        return Math.max(...v) 
    }
  
    console.log('max: ' + mxv(data, "actives"))
    var max = mxv(data, "actives");
    var maxRounded = (Math.ceil(max/100)*100).toFixed();
    var fifthOfRounded = maxRounded / 5
	// get color depending on population density value
	function getColor(d) {
		return  d > fifthOfRounded * 4  ? '#ff0303' :
				d > fifthOfRounded * 3  ? '#ff4e4e' :
				d > fifthOfRounded * 2  ? '#ff8181' :
				d > fifthOfRounded      ? '#ffb3b3' :
				d > 0                   ? '#ffe5e5' :
							              'transparent';
	}
//['#ff3333', 'orange', 'yellow','lime','aqua'],
	function style(feature) {
		return {
			weight: 2,
			opacity: 1,
			color: 'white',
			dashArray: '3',
			fillOpacity: 0.4,
			fillColor: getColor(feature.properties.confirmed)
		};
    }

    

    
	function highlightFeature(e) {
		var layer = e.target;
       
		layer.setStyle({
			weight: 5,
			color: '#666',
			dashArray: '',
			fillOpacity: 0.8
		});

		if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
			layer.bringToFront();
		}

        info.update(layer.feature.properties);
        
	}

	var geojson;

	function resetHighlight(e) {
		geojson.resetStyle(e.target);
        info.update();
        // $('#leafinfo').html('');
        // $('#WilayaChart').html('');

	}

	function zoomToFeature(e) {
        var layer = e.target;
        map2.fitBounds(e.target.getBounds());
        var wilayaInfo = getwilayaInfo(layer.feature.properties);
        var wilayaChart = getwilayaChart(layer.feature.properties);
        var wilayaChartSex = getwilayaChartSex(layer.feature.properties);
        
        $('#leafinfo').html(wilayaInfo);
        $('#WilayaChart').html(wilayaChart);
        $('#WilayaChartSex').html(wilayaChartSex);
	}

	function onEachFeature(feature, layer) {
		layer.on({
			mouseover: highlightFeature,
			mouseout: resetHighlight,
			click: zoomToFeature
		});
	}

	geojson = L.geoJson(limit_adm, {
		style: style,
		onEachFeature: onEachFeature
	}).addTo(map2);

	//map2.attributionControl.addAttribution('Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors');


	var legend = L.control({position: 'bottomleft'});

	legend.onAdd = function (map2) {

		var div = L.DomUtil.create('div', 'info legend'),
			grades = [1, fifthOfRounded, fifthOfRounded*2, fifthOfRounded*3, fifthOfRounded*4],
			labels = [],
			from, to;

		for (var i = 0; i < grades.length; i++) {
			from = grades[i];
			to = grades[i + 1];

			labels.push(
				'<i style="background:' + getColor(from + 1) + '"></i> ' +
				from + (to ? '&ndash;' + to :  '&ndash;' + maxRounded));
		}

		div.innerHTML = labels.join('<br>');
		return div;
	};

	legend.addTo(map2);
}